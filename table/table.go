package table

import (
	"fmt"
	"io"
	"strings"

	"github.com/bbrks/wrap"
)

// Table is made of rows.
type Table struct {
	widthOfCols []int
	rows        []interface{}
}

// New initializes a new table with minimal width of columns (optional).
func New(widthOfCols []int) *Table {
	return &Table{
		widthOfCols: widthOfCols,
	}
}

// AppendCells appends strings that will be rendered as cells.
func (t *Table) AppendCells(cells ...string) {
	t.rows = append(t.rows, cells)
}

// AppendText appends a text that will be wrapped and rendered as a single cell.
func (t *Table) AppendText(text string) {
	t.rows = append(t.rows, text)
}

type separator int

// AppendSeparator appends a row separator.
func (t *Table) AppendSeparator() {
	t.rows = append(t.rows, separator(0))
}

// Render renders the table.
func (t *Table) Render(writer io.Writer) {

	// Calculate width of columns
	widthOfCols := t.widthOfCols
	for _, row := range t.rows {
		if cells, ok := row.([]string); ok {
			// Initialize width of cols
			if widthOfCols == nil {
				widthOfCols = make([]int, len(cells))
			}

			// Update width of cols
			for i, cell := range cells {
				if i > len(widthOfCols)-1 {
					break // Ignore exceeding cells
				}
				if len(cell) > widthOfCols[i] {
					widthOfCols[i] = len(cell)
				}
			}
		}
	}
	numCols := len(widthOfCols)

	// Total, line width
	totalWidth := 3*len(widthOfCols) + 1 // cell borders
	for _, w := range widthOfCols {
		totalWidth += w // cell content and inner margins
	}
	lineWidth := totalWidth - 4

	// Render rows
	for _, row := range t.rows {
		switch row := row.(type) {
		case separator:
			// Render separator
			fmt.Fprintf(writer, "+%s+\n", strings.Repeat("-", totalWidth-2))

		case []string:
			// Render cells
			for i, cell := range row {
				if i > numCols-1 {
					break // Ingore exceeding cells
				}
				var pad string
				if npad := widthOfCols[i] - len(cell); npad > 0 {
					pad = strings.Repeat(" ", npad)
				}
				fmt.Fprintf(writer, "| %s%s ", cell, pad) // border, content, padding
			}
			fmt.Fprintf(writer, "|\n") // Right border

		case string:
			// Render line
			for _, line := range strings.Split(strings.TrimSpace(wrap.Wrap(row, lineWidth)), "\n") {
				var pad string
				if npad := lineWidth - len(line); npad > 0 {
					pad = strings.Repeat(" ", npad)
				}
				fmt.Fprintf(writer, "| %s%s |\n", line, pad) // border, content, padding, border
			}
		}
	}
}
