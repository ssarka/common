# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.9.0] - 2020-04-06
### Added

- Add `id` JSON field to vulnerabilities (!79)

## [v2.8.0] - 2020-3-31
### Changed

- CA bundle writer to append to existing files (!86)
- cacert.Import function to accept cacert.ImportOptions to specify where to write the CA certificate bundle (!86)
- command.Config now accepts cacert.ImportOptions (!86)

## [v2.7.0] - 2020-3-16
### Added

- Common logrus format (!73)

## [v2.6.2] - 2020-03-16
### Removed

- Remove unused structs and types introduced for DAST (!75)

## [v2.6.1] - 2020-03-12
### Changed

- Refactor the Go package used to import CA cert bundles (!83)

## [v2.6.0] - 2020-02-28
### Added

- Custom CA Cert support via the `--additional-ca-cert-bundle` and the `ADDITIONAL_CA_CERT_BUNDLE` env var (!76)

## [v2.5.7] - 2020-02-25
### Removed

- Remove undefined level from confidence and severity

## [v2.5.6] - 2020-01-15
### Changed

- Suppress the progress message from Docker Registry on pulling analyzer image (!65)

## [v2.5.5] - 2020-01-10
### Added

- Log when downloading, starting analyzers (!64)

## [v2.5.4] - 2020-01-06
### Added

- Support for `Red Hat Security Advisory (RHSA)` and `Oracle Linux Security Data` vulnerability types (!62)

## [v2.5.3] - 2019-11-07
### Added

- New `VulnerabilitiesAsReport` option for orchestrators, to be set to `true` to generate report v1 (!60)

## [v2.5.2] - 2019-10-15
### Added

- `image` in `.vulnerabilities[].location` report node: for vulnerabilities found by Container Scanning (!53)
- `operating_system` in `.vulnerabilities[].location` report node: for vulnerabilities found by Container Scanning (!53)
- Bump format version from `2.2` to `2.3`

## [v2.5.1] - 2019-09-19
### Fixed

- Force uid/guid when copying project directory for `nodejs-scan`, fixes missing read permissions

## [v2.5.0] - 2019-07-01
### Added

- `site_area` in `vulnerabilities[].location` report node: for vulnerabilities found by DAST (!39)
- New identifier type `wasc` which represents [WASC-ID](http://projects.webappsec.org/Threat-Classification-Reference-Grid) (!39)
- New identifier type `zap_plugin_id` which represents [ZAProxy plugin ID](https://github.com/zaproxy/zaproxy/blob/master/docs/scanners.md) (!39)
- `scanned_urls` as a top-level report node: details for URLs that were successfully scanned by DAST tool (!39)
- `io_error_urls` as a top-level report node: details for URLs scanned by DAST tool for which the target server did not respond (!39)

## [v2.4.2] - 2019-06-27
### Fixed
- `DS_EXCLUDED_PATHS` not applied to dependency files (!41)

## [v2.4.1] - 2019-06-04
### Added

- Sort dependency files and dependencies when merging reports (!36)

## [v2.4.0] - 2019-05-29
### Added

- `dependency_files` added to the report syntax (!35)

## [v2.3.0] - 2019-05-01
### Added

- `SAST_EXCLUDED_PATHS` and `DS_EXCLUDED_PATHS` to filter reports
  and remove vulnerabilities matching a set of excluded paths/globs

## [v2.2.0] - 2019-04-15
### Added

- `SeverityLevel` and `ConfidenceLevel` structs instead of `Level`

### Changed

- "Ignore" Severity level is now encoded as "Info"
- "Critical" Confidence level is now encoded as "Confirmed"

### Removed

- `Level` struct
- "Experimental" level for Severity

## [v2.1.6] - 2019-04-01
### Added
- Analyze sub-command

## [v2.1.5] - 2019-03-26
### Fixed
- Correct default analyzer image tag from latest to 2

## [v2.1.4] - 2019-03-20
### Fixed
- Fix report sorting for run execution to ensure consistent order

## [v2.1.3] - 2019-01-30
### Added
- Sub-sort vulnerabilities using compare key

## [v1.9.2] - 2019-01-22
### Fixed
- Null vulnerabilities when report is empty

## [v2.1.2] - 2019-01-10
### Fixed
- Null vulnerabilities when report is empty

## [v2.1.1] - 2019-01-10
### Fixed
- Merging of remediations

## [v2.1.0] - 2019-01-10
### Added
- Remediations field

## [v2.0.1] - 2019-01-04
### Fixed
- Path of nested modules

## [v2.0.0] - 2019-01-03
### Added
- V2 of Go module
- Format version to reports

### Changed
- Update documentation

## [v1.9.1] - 2019-12-18
### Added
- Remove duplicate issues/vulnerabilities

## [v1.9.0] - 2018-12-10
### Added
- Dependency to location, generate compare key & message for DS

### Changed
- Improve comment about Severity and Confidence levels

## [v1.8.1] - 2018-12-10
### Added
- Remote checks flag

## [v1.8.0] - 2018-12-05
### Added
- Import orchestrator, table

## [v1.7.0] - 2018-11-30
### Added
- Dependency Scanning support
- Set Docker entrypoint, cmd
- Redirect to os.Stdout, os.Stderr

### Changed
- Switch to semantic versioning

### Fixed
- Godoc, no markup

### Removed
- Match func, unused
- Versioning information, not accurate

## [v1.6.0] - 2018-09-21
### Added
- Report format compliant with Data Model constraints
- Match plugin to template
- Notice about using GitLab EE issue tracker in the Analyzer template
- Notice about using GitLab EE issue tracker

### Changed
- Correctly describe LICENSE

### Removed
- app.Email from template

## [v1.5.0] - 2018-06-05
### Added
- JSON unmarshaller to Level type
- More properties to SAST report

## [v1.4.0] - 2018-05-11
### Added
- Sort by priority

## [v1.3.0] - 2018-05-10
### Added
- Plugins search.MatchFunc functions

## [v1.2.0] - 2018-05-09
### Changed
- Prefix env var with ANAYZER_

## [v1.1.1] - 2018-05-09
### Added
- Truncate existing artifact file before writing

## [v1.1.0] - 2018-05-04
### Added
- Analyze the root directory

### Removed
- Error wrapper

## [v1.0.0] - 2018-04-25
### Added
- License
- Readme
- GitLab CI config
- Run sub-command
- Convert sub-command
- Make search command return project dir
- Handler error when closing file
- Exit with code 1 when match function fails

[v2.1.3]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/9f0ad391d73c2ee0458b7c977045361d5dd48da3
[v1.9.2]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/7e7754a36e12bab97c70bb41999843982d538f06
[v2.1.2]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/34fb051dad8bb17007b276293fac0a5268b8337c
[v2.1.1]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/7fea72e74c8e85d250749b554c67d1a5ef81aa6f
[v2.1.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/5a4674af2773c23fc8e5f8ace2d961403d0454e9
[v2.0.1]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/3d8a4f5ed42149c984438ac529691b84b5070613
[v2.0.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/fd777362c32473330c63661997c7d9d3b98cdad8
[v1.9.1]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/131c639c2176ff6e586af6dc96b90fd5f8e57201
[v1.9.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/d61d917d9ac37039417f58fad9de57bfed2d5941
[v1.8.1]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/b96aa2b758a75be1ba3ee2f436fbc5e585327ae2
[v1.8.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/ebb38e76bc5c5c223d12891b514e1970b92040f1
[v1.7.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/30c8d56e46c7be13a4831b2ca1fb97ad44ddc21b
[v1.6.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/5980b54c143aab1c25876fa70ea8572e65a53bb6
[v1.5.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/ff690b45b77632ee47f761fffec76c8000c55705
[v1.4.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/b4eb7dfa720ce805c85c8f8507bed0a76fdd0fd1
[v1.3.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/c53eebdd66d868f3000f9ac7a3140516682db76d
[v1.2.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/8c2db0688f43166c96df73a50d5862203d419fb0
[v1.1.1]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/6fb2fc46cf630126b5c6c92433fdfed645798ffd
[v1.1.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/6a3a2947ad0252fe851288d3f3c580842e3e213b
[v1.0.0]: https://gitlab.com/gitlab-org/security-products/analyzers/common/commit/b501a6ff249174c04f7268a7191b8b6d603ad003
