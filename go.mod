module gitlab.com/gitlab-org/security-products/analyzers/common/v2

require (
	github.com/kr/pretty v0.1.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/urfave/cli v1.22.1
	golang.org/x/sys v0.0.0-20191220142924-d4481acd189f // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

go 1.13
