package command

import (
	"io"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
)

// AnalyzeFunc is a type for a function that runs the analyzer command against
// the files in project dir and emits the analyzer's output for further processing.
type AnalyzeFunc func(c *cli.Context, path string) (io.ReadCloser, error)

// Analyze returns a cli sub-command that wraps the analyzing the project and generating the report.
func Analyze(cfg Config) cli.Command {
	return cli.Command{
		Name:      "analyze",
		Aliases:   []string{"a"},
		Usage:     "Analyze detected project and generate report",
		ArgsUsage: "<project-dir>",
		Flags:     append(cacert.NewFlags(), cfg.AnalyzeFlags...),
		Action: func(c *cli.Context) error {
			// check args
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}

			// import CA bundle
			if err := cacert.Import(c, cfg.CACertImportOptions); err != nil {
				return err
			}

			// input file
			input := c.Args().First()
			f, err := cfg.Analyze(c, input)
			if err != nil {
				return err
			}
			defer f.Close()

			if _, err := io.Copy(os.Stdout, f); err != nil {
				return err
			}
			return nil
		},
	}
}
