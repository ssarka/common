package command

import (
	"fmt"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
)

// Search returns a cli sub-command that implements project search.
func Search(cfg Config) cli.Command {
	return cli.Command{
		Name:      "search",
		Aliases:   []string{"s"},
		Usage:     "Search for compatible projects and return project directory",
		ArgsUsage: "<path>",
		Flags:     append(cacert.NewFlags(), search.NewFlags()...),
		Action: func(c *cli.Context) error {
			// check args
			if len(c.Args()) != 1 {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}
			rootPath := c.Args().First()

			// import CA bundle
			if err := cacert.Import(c, cfg.CACertImportOptions); err != nil {
				return err
			}

			// search
			opts := search.NewOptions(c)
			matchPath, err := search.New(cfg.Match, opts).Run(rootPath)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, matchPath)
			return nil
		},
	}
}
