package cacert

import (
	"os"
	"path/filepath"
)

// bundle combines a CA bundle with the path where it is imported.
type bundle struct {
	content string // content is the CA bundle to import.
	path    string // path is where the CA bundle is imported.
}

// Write writes the CA bundle to its import path.
// This is a no-op if the CA bundle is empty.
func (b bundle) write() error {
	// skip if bundle is empty
	if b.content == "" {
		return nil
	}

	// skip if no import path
	if b.path == "" {
		return nil
	}

	// create parent directory for the import path
	if err := os.MkdirAll(filepath.Dir(b.path), 0755); err != nil {
		return err
	}

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(b.path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write([]byte(b.content))
	return err
}
