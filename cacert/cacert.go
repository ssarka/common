// Package cacert provides code to import CA certificate bundles.
// It is configured by CLI flags and environment variables.
package cacert

import (
	"github.com/urfave/cli"
)

const (
	// EnvVarBundleContent is the env var for the CA bundle to import
	EnvVarBundleContent = "ADDITIONAL_CA_CERT_BUNDLE"

	// FlagBundleContent is the CLI flag for the CA bundle to import
	FlagBundleContent = "additional-ca-cert-bundle"

	// defaultBundlePath is the default import path of the CA bundle
	defaultBundlePath = "/etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem"
)

// NewFlags returns CLI flags to configure the import of a CA bundle.
func NewFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   FlagBundleContent,
			Usage:  "Additional CA certs bundle to import",
			EnvVar: EnvVarBundleContent,
		},
	}
}

// ImportOptions provides a way to specify optional arguments for the Import function.
type ImportOptions struct {
	Path string // Path specifies where to write the additional CA cert bundle.
}

// Import writes the CA bundle to its import path.
// This is a no-op if the CA bundle is empty.
func Import(c *cli.Context, opts ImportOptions) error {
	path := opts.Path
	if path == "" {
		path = defaultBundlePath
	}

	return bundle{
		content: c.String(FlagBundleContent),
		path:    path,
	}.write()
}
