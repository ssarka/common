package logutil

import (
	"fmt"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

// Formatter is used to by the logrus package to provide a custom logger
type Formatter struct {
	Project         string
	TimestampFormat string
}

const (
	colorDebug = "\x1b[0;34m%s\x1b[0m"
	colorInfo  = "\x1b[0;32m%s\x1b[0m"
	colorWarn  = "\x1b[0;33m%s\x1b[0m"
	colorError = "\x1b[0;31m%s\x1b[0m"
	colorFatal = "\x1b[0;31m%s\x1b[0m"
)

// Format creates a custom log formatter so we can colorize and format the output
func (f *Formatter) Format(entry *logrus.Entry) ([]byte, error) {
	formattedLevel := strings.ToUpper(entry.Level.String()[0:4])

	colorFormatString := func() string {
		switch entry.Level {
		case logrus.InfoLevel:
			return colorInfo
		case logrus.WarnLevel:
			return colorWarn
		case logrus.ErrorLevel:
			return colorError
		case logrus.DebugLevel:
			return colorDebug
		case logrus.FatalLevel:
			return colorFatal
		}

		return colorError
	}()

	timestampFormat := time.RFC1123
	if f.TimestampFormat != "" {
		timestampFormat = f.TimestampFormat
	}

	logEntry := fmt.Sprintf("[%s] [%s] ▶ %s\n", formattedLevel, entry.Time.Format(timestampFormat), entry.Message)
	if f.Project != "" {
		logEntry = fmt.Sprintf("[%s] [%s] [%s] ▶ %s\n", formattedLevel, f.Project, entry.Time.Format(timestampFormat), entry.Message)
	}

	coloredLogEntry := fmt.Sprintf(colorFormatString, logEntry)

	return []byte(coloredLogEntry), nil
}
