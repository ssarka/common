package issue

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

var testReport = Report{
	Version: Version{2, 0},
	Vulnerabilities: []Issue{
		{
			Category:   CategoryDependencyScanning,
			Message:    "Vulnerability in io.netty/netty",
			CompareKey: "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
			Scanner: Scanner{
				ID:   "gemnasium",
				Name: "Gemnasium",
			},
			Location: Location{
				File: "app/pom.xml",
				Dependency: Dependency{
					Package: Package{
						Name: "io.netty/netty",
					},
					Version: "3.9.1.Final",
				},
			},
			Identifiers: []Identifier{
				CVEIdentifier("CVE-2018-1234"),
			},
		},
	},
	Remediations: []Remediation{
		{
			Fixes: []Ref{
				{
					CompareKey: "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
					ID:         "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3",
				},
			},
			Summary: "Upgrade to netty 3.9.2.Final",
			Diff:    "diff (base64 encoded) placeholder",
		},
	},
	DependencyFiles: []DependencyFile{
		{
			Path:           "app/pom.xml",
			PackageManager: PackageManagerMaven,
			Dependencies: []Dependency{
				{Package: Package{Name: "io.netty/netty"}, Version: "3.9.2.Final"},
			},
		},
	},
}

var testReportJSON = `{
  "version": "2.0",
  "vulnerabilities": [
    {
      "id": "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3",
      "category": "dependency_scanning",
      "message": "Vulnerability in io.netty/netty",
      "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "io.netty/netty"
          },
          "version": "3.9.1.Final"
        }
      },
      "identifiers": [
        {
          "type": "cve",
          "name": "CVE-2018-1234",
          "value": "CVE-2018-1234",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
        }
      ]
    }
  ],
  "remediations": [
    {
      "fixes": [
        {
          "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
          "id": "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3"
        }
      ],
      "summary": "Upgrade to netty 3.9.2.Final",
      "diff": "diff (base64 encoded) placeholder"
    }
  ],
  "dependency_files": [
    {
      "path": "app/pom.xml",
      "package_manager": "maven",
      "dependencies": [
        {
          "package": {
            "name": "io.netty/netty"
          },
          "version": "3.9.2.Final"
        }
      ]
    }
  ]
}`

var testEmptyReport = func() Report {
	r := NewReport()
	r.DependencyFiles = nil // emulate SAST report
	return r
}()

var testEmptyReportJSON = `{
  "version": "2.4",
  "vulnerabilities": [],
  "remediations": []
}`

func TestReport(t *testing.T) {
	testCases := []struct {
		Name string
		Report
		ReportJSON string
	}{
		{
			Name:       "GenericReport",
			Report:     testReport,
			ReportJSON: testReportJSON,
		},
		{
			Name:       "EmptyReport",
			Report:     testEmptyReport,
			ReportJSON: testEmptyReportJSON,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			t.Run("MarshalJSON", func(t *testing.T) {
				b, err := json.Marshal(tc.Report)
				if err != nil {
					t.Fatal(err)
				}

				var buf bytes.Buffer
				json.Indent(&buf, b, "", "  ")
				got := buf.String()

				want := tc.ReportJSON
				if got != want {
					t.Errorf("Wrong JSON output. Expected:\n%s\nBut got:\n%s", want, got)
				}
			})

			t.Run("UnmarshalJSON", func(t *testing.T) {
				var got Report
				if err := json.Unmarshal([]byte(tc.ReportJSON), &got); err != nil {
					t.Fatal(err)
				}

			})
		})
	}

	t.Run("ExcludePaths", func(t *testing.T) {
		report := Report{
			Version: CurrentVersion(),
			Vulnerabilities: []Issue{
				{
					Name:       "R1/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R1/critical/ckey",
					Location:   Location{File: "api/pom.xml"},
				},
				{
					Name:       "R2/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/critical/ckey",
					Location:   Location{File: "api/pom.xml"},
				},
				{
					Name:       "R3/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/critical/ckey",
					Location:   Location{File: "web/pom.xml"},
				},
				{
					Name:       "R1/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R1/low/ckey",
					Location:   Location{File: "model/pom.xml"},
				},
				{
					Name:       "R2/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
				{
					Name:       "R3/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R1/critical/ckey"}}, // excluded
					Summary: "Upgrade dependency to fix R1/critical",
					Diff:    "diff fixing R1/critical",
				},
				{
					Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
			DependencyFiles: []DependencyFile{
				{
					Path:           "api/pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "ognl/ognl"}, Version: "3.1.8"},
						{Package: Package{Name: "org.apache.struts/struts2-core"}, Version: "2.5.1"},
						{Package: Package{Name: "org.freemarker/freemarker"}, Version: "2.3.23"},
					},
				},
				{
					Path:           "model/pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "org.apache.logging.log4j/log4j-api"}, Version: "2.8.2"},
						{Package: Package{Name: "org.apache.logging.log4j/log4j-core"}, Version: "2.8.2"},
					},
				},
				{
					Path:           "pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
				{
					Path:           "web/pom.xml",
					PackageManager: "bundler",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.2"},
						{Package: Package{Name: "org.codehaus.woodstox/stax2-api"}, Version: "3.1.4"},
					},
				},
			},
		}

		want := Report{
			Version: CurrentVersion(),
			Vulnerabilities: []Issue{
				{
					Name:       "R3/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/critical/ckey",
					Location:   Location{File: "web/pom.xml"},
				},
				{
					Name:       "R2/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
				{
					Name:       "R3/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
			DependencyFiles: []DependencyFile{
				{
					Path:           "pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
				{
					Path:           "web/pom.xml",
					PackageManager: "bundler",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.2"},
						{Package: Package{Name: "org.codehaus.woodstox/stax2-api"}, Version: "3.1.4"},
					},
				},
			},
		}

		var isExcluded = func(path string) bool {
			switch path {
			case "api/pom.xml", "model/pom.xml":
				return true
			default:
				return false
			}
		}

		report.ExcludePaths(isExcluded)
		if !reflect.DeepEqual(want, report) {
			t.Errorf("Wrong result. Expected:\n%#v\nGot:\n%#v", want, report)
		}
	})
}

func TestNewReport(t *testing.T) {
	got := NewReport().Version
	want := CurrentVersion()
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong version. Expecting %v but got %v", want, got)
	}
}

// NOTE: Sort is tested when testing MergeReports

func TestMergeReports(t *testing.T) {
	location := Location{File: "app/Gemfile.lock"}
	ids := []Identifier{CVEIdentifier("CVE-2018-14404")}

	reports := []Report{
		{
			Version: Version{Major: 2, Minor: 3},
			Vulnerabilities: []Issue{
				{
					Name:       "R1/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R1/low/ckey",
				},
				{
					Name:        "R1/critical",
					Severity:    SeverityLevelCritical,
					Confidence:  ConfidenceLevelUnknown,
					Location:    location,
					Identifiers: ids,
					CompareKey:  "R1/critical/ckey",
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R1/critical/ckey"}},
					Summary: "Upgrade dependency to fix R1/critical",
					Diff:    "diff fixing R1/critical",
				},
			},
			DependencyFiles: []DependencyFile{
				{
					Path:           "pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
			},
		},
		{
			Version: Version{Major: 2, Minor: 4},
			Vulnerabilities: []Issue{
				{
					Name:       "R2/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/low/ckey",
				},
				{
					Name:       "R2/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/critical/ckey",
				},
			},
		},
		{
			Version: Version{Major: 2, Minor: 5},
			Vulnerabilities: []Issue{
				{
					Name:       "R3/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/low/ckey",
				},
				{
					Name:       "R3/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/critical/ckey",
				},
				{
					Name:        "R3/critical/dup",
					Severity:    SeverityLevelCritical,
					Confidence:  ConfidenceLevelUnknown,
					Location:    location,
					Identifiers: ids,
					CompareKey:  "R3/critical/ckey",
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
			DependencyFiles: []DependencyFile{
				{
					Path:           "api/pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "org.apache.struts/struts2-core"}, Version: "2.5.1"},
						{Package: Package{Name: "org.freemarker/freemarker"}, Version: "2.3.23"},
						{Package: Package{Name: "ognl/ognl"}, Version: "3.1.8"},
					},
				},
				{
					Path:           "model/pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "org.apache.logging.log4j/log4j-core"}, Version: "2.8.2"},
						{Package: Package{Name: "org.apache.logging.log4j/log4j-api"}, Version: "2.8.2"},
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
				{
					Path:           "web/pom.xml",
					PackageManager: "bundler", // other package manager
					Dependencies: []Dependency{
						{Package: Package{Name: "org.codehaus.woodstox/stax2-api"}, Version: "3.1.4"},
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.2"}, // other version
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"}, // duplicate
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
			},
		},
	}

	want := Report{
		Version: CurrentVersion(),
		Vulnerabilities: []Issue{
			{
				Name:        "R1/critical",
				Severity:    SeverityLevelCritical,
				Confidence:  ConfidenceLevelUnknown,
				Location:    location,
				Identifiers: ids,
				CompareKey:  "R1/critical/ckey",
			},
			{
				Name:       "R2/critical",
				Severity:   SeverityLevelCritical,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R2/critical/ckey",
			},
			{
				Name:       "R3/critical",
				Severity:   SeverityLevelCritical,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R3/critical/ckey",
			},
			{
				Name:       "R1/low",
				Severity:   SeverityLevelLow,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R1/low/ckey",
			},
			{
				Name:       "R2/low",
				Severity:   SeverityLevelLow,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R2/low/ckey",
			},
			{
				Name:       "R3/low",
				Severity:   SeverityLevelLow,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R3/low/ckey",
			},
		},
		Remediations: []Remediation{
			{
				Fixes:   []Ref{{CompareKey: "R1/critical/ckey"}},
				Summary: "Upgrade dependency to fix R1/critical",
				Diff:    "diff fixing R1/critical",
			},
			{
				Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
				Summary: "Upgrade dependency to fix R3/critical",
				Diff:    "diff fixing R3/critical",
			},
		},
		DependencyFiles: []DependencyFile{
			{
				Path:           "api/pom.xml",
				PackageManager: "maven",
				Dependencies: []Dependency{
					{Package: Package{Name: "ognl/ognl"}, Version: "3.1.8"},
					{Package: Package{Name: "org.apache.struts/struts2-core"}, Version: "2.5.1"},
					{Package: Package{Name: "org.freemarker/freemarker"}, Version: "2.3.23"},
				},
			},
			{
				Path:           "model/pom.xml",
				PackageManager: "maven",
				Dependencies: []Dependency{
					{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					{Package: Package{Name: "org.apache.logging.log4j/log4j-api"}, Version: "2.8.2"},
					{Package: Package{Name: "org.apache.logging.log4j/log4j-core"}, Version: "2.8.2"},
				},
			},
			{
				Path:           "pom.xml",
				PackageManager: "maven",
				Dependencies: []Dependency{
					{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
				},
			},
			{
				Path:           "web/pom.xml",
				PackageManager: "bundler",
				Dependencies: []Dependency{
					{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.2"},
					{Package: Package{Name: "org.codehaus.woodstox/stax2-api"}, Version: "3.1.4"},
				},
			},
		},
	}

	got := MergeReports(reports...)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nGot:\n%#v", want, got)
	}
}
