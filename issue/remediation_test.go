package issue

import (
	"reflect"
	"testing"
)

func TestNewRef(t *testing.T) {
	vuln := Issue{
		CompareKey: "debian:10:nghttp2:CVE-2019-9511",
	}

	want := Ref{
		ID:         "1559da8161d0b5d881fc8e9b215464d0a5a8a32dd408bfd845aa3a5b5dcf9a3e",
		CompareKey: "debian:10:nghttp2:CVE-2019-9511",
	}

	got := NewRef(vuln)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", want, got)
	}
}
