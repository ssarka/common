package issue

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

const (
	// VersionMajor is the major number of the current version
	VersionMajor = 2
	// VersionMinor is the minor number of the current version
	VersionMinor = 4

	// DefaultVersionMajor is the major number when version is not given
	DefaultVersionMajor = 1
	// DefaultVersionMinor is the minor number when version is not given
	DefaultVersionMinor = 4
)

// CurrentVersion returns the current version of the report syntax.
func CurrentVersion() Version {
	return Version{VersionMajor, VersionMinor}
}

// Version represents the version of the report syntax.
type Version struct {
	Major uint
	Minor uint
}

// MarshalJSON encodes a version to JSON.
func (v Version) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.String())
}

// UnmarshalJSON decodes a version.
func (v *Version) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}

	// set to default version if empty string
	if s == "" {
		v.Major = DefaultVersionMajor
		v.Minor = DefaultVersionMinor
		return nil
	}

	// parse major number
	parts := strings.SplitN(s, ".", 2)
	major, err := strconv.ParseUint(parts[0], 10, 32)
	if err != nil {
		return err
	}
	v.Major = uint(major)

	// parse minor number
	if len(parts) > 1 {
		minor, err := strconv.ParseUint(parts[1], 10, 32)
		if err != nil {
			return err
		}
		v.Minor = uint(minor)
	}

	return nil
}

// String turns the version into a "MAJOR.MINOR".
func (v Version) String() string {
	return fmt.Sprintf("%d.%d", v.Major, v.Minor)
}
