package issue

import (
	"reflect"
	"testing"
)

func TestCurrentVersion(t *testing.T) {
	want := Version{Major: 2, Minor: 4}
	got := CurrentVersion()
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expecting %v but got %v", want, got)
	}
}

func TestVersion(t *testing.T) {
	t.Run("String", func(t *testing.T) {
		want := "12.34"
		got := Version{Major: 12, Minor: 34}.String()
		if got != want {
			t.Errorf("Wrong result. Expecting %s but got %s", want, got)
		}
	})

	t.Run("MarshalJSON", func(t *testing.T) {
		want := []byte(`"12.34"`)
		got, err := Version{Major: 12, Minor: 34}.MarshalJSON()
		if err != nil {
			t.Fatal(err)
		}
		if string(got) != string(want) {
			t.Errorf("Wrong result. Expecting %s but got %s", string(want), string(got))
		}
	})

	t.Run("UnarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name  string
			Input string
			Want  Version
		}{
			{
				Name:  "Empty",
				Input: `null`,
				Want:  Version{Major: DefaultVersionMajor, Minor: DefaultVersionMinor},
			},
			{
				Name:  "Major",
				Input: `"12"`,
				Want:  Version{Major: 12},
			},
			{
				Name:  "Major.Minor",
				Input: `"12.34"`,
				Want:  Version{Major: 12, Minor: 34},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				var got Version
				err := got.UnmarshalJSON([]byte(tc.Input))
				if err != nil {
					t.Fatal(err)
				}
				if got != tc.Want {
					t.Errorf("Wrong result. Expecting %v but got %v", tc.Want, got)
				}
			})
		}
	})
}
