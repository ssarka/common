package issue

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

var testSASTIssue = Issue{
	Category:    CategorySast,
	Name:        "Possible command injection",
	Message:     "Possible command injection in application_controller",
	Description: "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
	CompareKey:  "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
	Location: Location{
		File:      "app/controllers/application_controller.rb",
		LineStart: 831,
		LineEnd:   832,
	},
	Scanner: Scanner{
		ID:   "brakeman",
		Name: "Brakeman",
	},
	Severity:   SeverityLevelMedium,
	Confidence: ConfidenceLevelHigh,
	Solution:   "Use the system(command, parameters) method which passes command line parameters safely.",
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
	Links: []Link{
		{
			Name: "Awesome-security blog post",
			URL:  "https://example.com/blog-post",
		},
		{
			URL: "https://example.com/another-blog-post",
		},
	},
}

const testSASTIssueJSON = `{
  "id": "1f99a02c93deaa1a36bfc2a2066eb9c536121637dac438c88b5f314c42cb023b",
  "category": "sast",
  "name": "Possible command injection",
  "message": "Possible command injection in application_controller",
  "description": "The ciphertext produced is susceptible to alteration by an adversary. This mean that the cipher provides no wayto detect that the data has been tampered with. If the ciphertext can be controlled by an attacker, it couldbe altered without detection.\n\nThe solution is to used a cipher that includes a Hash basedMessage Authentication Code (HMAC) to sign the data. Combining a HMAC function to the existing cipher is proneto error [1] ( http://www.thoughtcrime.org/blog/the-cryptographic-doom-principle/ ).Specifically, it is always recommended that you be able to verify the HMAC first, and only if the data is unmodified,do you then perform any cryptographic functions on the data.\n\nThe following modes are vulnerablebecause they don't provide a HMAC:",
  "cve": "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
  "severity": "Medium",
  "confidence": "High",
  "solution": "Use the system(command, parameters) method which passes command line parameters safely.",
  "scanner": {
    "id": "brakeman",
    "name": "Brakeman"
  },
  "location": {
    "file": "app/controllers/application_controller.rb",
    "start_line": 831,
    "end_line": 832,
    "dependency": {
      "package": {}
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ],
  "links": [
    {
      "name": "Awesome-security blog post",
      "url": "https://example.com/blog-post"
    },
    {
      "url": "https://example.com/another-blog-post"
    }
  ]
}`

var testDependencyScanningVulnerability = DependencyScanningVulnerability{Issue{
	Category: CategoryDependencyScanning,
	Scanner: Scanner{
		ID:   "gemnasium",
		Name: "Gemnasium",
	},
	Location: Location{
		File: "app/pom.xml",
		Dependency: Dependency{
			Package: Package{
				Name: "io.netty/netty",
			},
			Version: "3.9.1.Final",
		},
	},
	Identifiers: []Identifier{
		CVEIdentifier("CVE-2018-1234"),
	},
}}

const testDependencyScanningVulnerabilityJSON = `{
  "id": "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3",
  "category": "dependency_scanning",
  "message": "Vulnerability in io.netty/netty",
  "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
  "scanner": {
    "id": "gemnasium",
    "name": "Gemnasium"
  },
  "location": {
    "file": "app/pom.xml",
    "dependency": {
      "package": {
        "name": "io.netty/netty"
      },
      "version": "3.9.1.Final"
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ]
}`

const testDependencyScanningIssueJSON = `{
  "category": "dependency_scanning",
  "scanner": {
    "id": "gemnasium",
    "name": "Gemnasium"
  },
  "location": {
    "file": "app/pom.xml",
    "dependency": {
      "package": {
        "name": "io.netty/netty"
      },
      "version": "3.9.1.Final"
    }
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2018-1234",
      "value": "CVE-2018-1234",
      "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
    }
  ]
}`

var testContainerScanningIssue = Issue{
	Category:    CategoryContainerScanning,
	Message:     "CVE-2019-9511 in nghttp2",
	Description: "Some HTTP/2 implementations are vulnerable to window size manipulation and stream prioritization manipulation, potentially leading to a denial of service. The attacker requests a large amount of data from a specified resource over multiple streams. They manipulate window size and stream priority to force the server to queue the data in 1-byte chunks. Depending on how efficiently this data is queued, this can consume excess CPU, memory, or both.",
	CompareKey:  "debian:10:nghttp2:CVE-2019-9511",
	Severity:    SeverityLevelHigh,
	Solution:    "Upgrade nghttp2 from 1.36.0-2 to 1.36.0-2+deb10u1",
	Scanner: Scanner{
		ID:   "klar",
		Name: "klar",
	},
	Location: Location{
		Dependency: Dependency{
			Package: Package{
				Name: "nghttp2",
			},
			Version: "1.36.0-2",
		},
		OperatingSystem: "debian:10",
		Image:           "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e",
	},
	Identifiers: []Identifier{
		{
			Type:  "cve",
			Name:  "CVE-2019-9511",
			Value: "CVE-2019-9511",
			URL:   "https://security-tracker.debian.org/tracker/CVE-2019-9511",
		},
	},
	Links: []Link{
		{
			Name: "",
			URL:  "https://security-tracker.debian.org/tracker/CVE-2019-9511",
		},
	},
}

const testContainerScanningIssueJSON = `{
  "id": "502b291f757825a98f6d5a4aef896a890b0527511754e72fcf1223c8c753921e",
  "category": "container_scanning",
  "message": "CVE-2019-9511 in nghttp2",
  "description": "Some HTTP/2 implementations are vulnerable to window size manipulation and stream prioritization manipulation, potentially leading to a denial of service. The attacker requests a large amount of data from a specified resource over multiple streams. They manipulate window size and stream priority to force the server to queue the data in 1-byte chunks. Depending on how efficiently this data is queued, this can consume excess CPU, memory, or both.",
  "cve": "debian:10:nghttp2:CVE-2019-9511",
  "severity": "High",
  "solution": "Upgrade nghttp2 from 1.36.0-2 to 1.36.0-2+deb10u1",
  "scanner": {
    "id": "klar",
    "name": "klar"
  },
  "location": {
    "dependency": {
      "package": {
        "name": "nghttp2"
      },
      "version": "1.36.0-2"
    },
    "operating_system": "debian:10",
    "image": "registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e"
  },
  "identifiers": [
    {
      "type": "cve",
      "name": "CVE-2019-9511",
      "value": "CVE-2019-9511",
      "url": "https://security-tracker.debian.org/tracker/CVE-2019-9511"
    }
  ],
  "links": [
    {
      "url": "https://security-tracker.debian.org/tracker/CVE-2019-9511"
    }
  ]
}`

const testEmptyIssueJSON = `{
  "id": "6c5c554c84f21e91c9bdf78dc2349458ce67e507e556b3d6ef34424d9dd95bbe",
  "category": "",
  "cve": "",
  "scanner": {
    "id": "",
    "name": ""
  },
  "location": {
    "dependency": {
      "package": {}
    }
  },
  "identifiers": null
}`

func TestIssue(t *testing.T) {
	t.Run("ID", func(t *testing.T) {
		var tcs = []struct {
			Name          string
			Vulnerability Issue
			ID            string
		}{
			{
				Name:          "SAST",
				Vulnerability: testSASTIssue,
				ID:            "1f99a02c93deaa1a36bfc2a2066eb9c536121637dac438c88b5f314c42cb023b",
			},
			{
				Name:          "DS",
				Vulnerability: testDependencyScanningVulnerability.ToIssue(),
				ID:            "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3",
			},
			{
				Name:          "Empty",
				Vulnerability: Issue{},
				ID:            "6c5c554c84f21e91c9bdf78dc2349458ce67e507e556b3d6ef34424d9dd95bbe",
			},
			{
				Name:          "CS",
				Vulnerability: testContainerScanningIssue,
				ID:            "502b291f757825a98f6d5a4aef896a890b0527511754e72fcf1223c8c753921e",
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				got := tc.Vulnerability.ID()
				want := tc.ID
				if got != want {
					t.Errorf("Wrong result. Expected %v but got %v", want, got)
				}
			})
		}
	})

	t.Run("MarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name          string
			Vulnerability Issue
			JSON          string
		}{
			{
				Name:          "SAST",
				Vulnerability: testSASTIssue,
				JSON:          testSASTIssueJSON,
			},
			{
				Name:          "DS",
				Vulnerability: testDependencyScanningVulnerability.ToIssue(),
				JSON:          testDependencyScanningVulnerabilityJSON,
			},
			{
				Name:          "Empty",
				Vulnerability: Issue{},
				JSON:          testEmptyIssueJSON,
			},
			{
				Name:          "CS",
				Vulnerability: testContainerScanningIssue,
				JSON:          testContainerScanningIssueJSON,
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				b, err := json.Marshal(tc.Vulnerability)
				if err != nil {
					t.Fatal(err)
				}

				var buf bytes.Buffer
				json.Indent(&buf, b, "", "  ")
				got := buf.String()

				if got != tc.JSON {
					t.Errorf("Wrong result. Expected:\n%#v\nBut got:\n%#v", tc.JSON, got)
				}
			})
		}
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			Name  string
			JSON  string // JSON encoded vulnerability
			Issue Issue  // Issue to be JSON decoded
		}{
			{
				Name:  "SAST",
				JSON:  testSASTIssueJSON,
				Issue: testSASTIssue,
			},
			{
				Name:  "DS",
				JSON:  testDependencyScanningIssueJSON,
				Issue: testDependencyScanningVulnerability.Issue,
			},
			{
				Name:  "ContainerScanning",
				JSON:  testContainerScanningIssueJSON,
				Issue: testContainerScanningIssue,
			},
			{
				Name:  "Empty",
				JSON:  testEmptyIssueJSON,
				Issue: Issue{},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				var got Issue
				if err := json.Unmarshal([]byte(tc.JSON), &got); err != nil {
					t.Fatal(err)
				}

				if !reflect.DeepEqual(got, tc.Issue) {
					t.Errorf("Wrong unmarshalled Issue. Expected:\n%#v\nBut got:\n%#v", tc.Issue, got)
				}
			})
		}
	})
}
